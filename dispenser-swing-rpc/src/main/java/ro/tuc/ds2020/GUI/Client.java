package ro.tuc.ds2020.GUI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.server.MessengerService;
import ro.tuc.ds2020.server.ResponseMedication;
import ro.tuc.ds2020.server.TakenPill;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;


public class Client extends JFrame{
    public JPanel myPannel = new JPanel();
    private JTable table1 = new JTable();
    private JButton button1 = new JButton();
    private List<ResponseMedication> responseMedication;
    private MessengerService messengerService;

    @Autowired
    public Client(List<ResponseMedication> responseMedication, MessengerService messengerService) throws RemoteException {
        this.responseMedication = responseMedication;
        this.messengerService = messengerService;

        System.out.println(this.messengerService.sendMessage("ALOHAAAAAAAAAAA"));

        createUIComponents();

        myPannel.add(table1);
        myPannel.add(button1);

        button1.addActionListener(new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("AM LUAT PASTILA!!!!!!!!!!!!!!!!!!!!!!");

                int row = table1.getSelectedRow();
                if (row == -1) {
                    System.out.println("ALOOOOO1");
                    JOptionPane.showMessageDialog(null, "Select a medication first!");
                } else {
                    System.out.println("ALOOOOO2");
                    String hourString = table1.getModel().getValueAt(row, 3).toString();
                    LocalTime hour = LocalTime.parse(hourString);
                    String medicationName = table1.getModel().getValueAt(row, 0).toString();

                    System.out.println("local --> " + LocalTime.now().plusHours(2));

                    if (LocalTime.now().plusHours(2).isBefore(hour)) {
                        JOptionPane.showMessageDialog(null, "Too early!");
                    } else {
                        System.out.println("la ce ora tre sa iau pastila: " + hour);
                        System.out.println("pana cand: " + hour.plusMinutes(300));

                        if (LocalTime.now().plusHours(2).isBefore(hour.plusMinutes(270))) {
                            System.out.println("ALOOOOO3");
                            TakenPill takenPill = new TakenPill(true);
                            try {
                                JOptionPane.showMessageDialog(null, notifyTakenPill(takenPill));
                                ((DefaultTableModel)table1.getModel()).removeRow(row);
                            } catch (RemoteException remoteException) {
                                remoteException.printStackTrace();
                            }
                        } else {
                            System.out.println("ALOOOOO4");
                        }
                    }
                }

            }
        });
    }

    public String notifyTakenPill(TakenPill takenPill) throws RemoteException {
        return this.messengerService.pillTaken(takenPill);
    }

    private void createUIComponents() {

        // create button
        button1.setText("Taken");

        // create table
        JTable table = new JTable();
        DefaultTableModel model = new DefaultTableModel();

        Vector<Object> header = new Vector<Object>();
        header.add("Medication");
        header.add("Starting Date");
        header.add("Ending Date");
        header.add("Hour");
        model.setColumnIdentifiers(header);

        for(ResponseMedication i : responseMedication) {

            Vector<Object> dataTable = new Vector<Object>();
            dataTable.add(i.getName());
            dataTable.add(i.getIntervalStart());
            dataTable.add(i.getIntervalEnd());
            dataTable.add(i.getHour());

            model.addRow(dataTable);
        }
        table.setModel(model);
        this.table1 = table;
    }
}
