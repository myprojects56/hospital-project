package ro.tuc.ds2020;

import com.google.gson.Gson;
import com.googlecode.jsonrpc4j.IJsonRpcClient;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImplExporter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.GUI.Client;
import ro.tuc.ds2020.server.MessengerService;
import ro.tuc.ds2020.server.ResponseMedication;


import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@SpringBootApplication
@Validated
@EnableScheduling
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Ds2020Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Ds2020Application.class);
    }

//    @Bean
//    RmiProxyFactoryBean rmiProxi() {
//        RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
//        bean.setServiceInterface(MessengerService.class);
//        bean.setServiceUrl("rmi://localhost:1099/helloworldrmi");
//
//        return bean;
//    }



    public static void main(String[] args) throws IOException, InterruptedException {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(Ds2020Application.class, args);
        System.setProperty("java.awt.headless", "false"); //Disables headless

        IJsonRpcClient client;
        client = new JsonRpcHttpClient(new URL("http://localhost:8080/hello"));

        MessengerService messengerService = ProxyUtil.createClientProxy(Ds2020Application.class.getClassLoader(), MessengerService.class, client);

        String medications = messengerService.sendPatientId("7a73aeb9-17d8-47bf-9a65-aadb41b37f69");
        System.out.println(medications);

        ResponseMedication[] med;
        Gson result = new Gson();
        med = result.fromJson(medications, ResponseMedication[].class);

        List<ResponseMedication> medicationPlans = new ArrayList<>();
        for (ResponseMedication r: med) {
            medicationPlans.add(r);
        }

        JFrame frame = new JFrame("Client");
        frame.setContentPane(new Client(medicationPlans, messengerService).myPannel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
