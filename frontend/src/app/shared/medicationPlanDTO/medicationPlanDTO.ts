export class MedicationPlanDTO {
    id: string;
    intervalStart: Date;
    intervalEnd: Date;
    partOfDay: String;
    period: String;
    doctor: String;
    patient: String;
    medication: String;
}