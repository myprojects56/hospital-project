import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { CaregiverDTO } from './caregiverDTO/caregiverDTO.model';
import { HttpService } from './http.service';

@Injectable()
export class CaregiverService {
  private httpService: HttpService;
  
  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public add(caregiverDTO: CaregiverDTO): Observable<string> {
    return this.httpService.post('/caregiver', caregiverDTO);
  }

  public getAll(): Observable<CaregiverDTO[]> {
      return this.httpService.get('/caregiver');
  }

  public update(caregiverDTO: CaregiverDTO): Observable<CaregiverDTO> {
    return this.httpService.post('/caregiver/update', caregiverDTO);
  }

  public delete(id: string): Observable<string> {
    return this.httpService.delete(`/caregiver/${id}`);
  }

  public getById(id: string): Observable<CaregiverDTO> {
    return this.httpService.get(`/caregiver/${id}`);
  }
}