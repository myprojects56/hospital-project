import { CaregiverDTO } from '../caregiverDTO/caregiverDTO.model';
import { DoctorDTO } from '../doctorDTO/doctorDTO.model';
import { PatientDTO } from '../patientDTO/patientDTO.model';

export class ResponseDTO {
    message: string;
    doctorDTO: DoctorDTO;
    patientDTO: PatientDTO;
    caregiverDTO: CaregiverDTO;
}