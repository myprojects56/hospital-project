export class PatientDTO {
    id: string;
    name: string;
    birthDate: Date;
    gender: string;
    address: string;
    medicalRecord: string;
    doctor: string;
    caregiver: string;
}