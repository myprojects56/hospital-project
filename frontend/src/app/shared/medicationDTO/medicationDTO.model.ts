export class MedicationDTO {
    id: string;
    name: string;
    sideEffects: string;
    dose: number;
}