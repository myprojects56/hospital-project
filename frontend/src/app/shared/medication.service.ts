import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { MedicationDTO } from './medicationDTO/medicationDTO.model';

@Injectable()
export class MedicationService {
  private httpService: HttpService;
  
  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public add(medicationDTO: MedicationDTO): Observable<string> {
    return this.httpService.post('/medication', medicationDTO);
  }

  public getAll(): Observable<MedicationDTO[]> {
      return this.httpService.get('/medication');
  }

  public update(medicationDTO: MedicationDTO): Observable<MedicationDTO> {
    return this.httpService.post('/medication/update', medicationDTO);
  }

  public delete(id: string): Observable<string> {
    return this.httpService.delete(`/medication/${id}`);
  }

  public getById(id: string): Observable<MedicationDTO> {
    return this.httpService.get(`/medication/${id}`);
  }
}