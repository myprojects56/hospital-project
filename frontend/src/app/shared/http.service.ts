import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl: string = `https://ds-backend-malina.herokuapp.com`;

@Injectable()
export class HttpService {
  private httpClient: HttpClient;

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public get(url: string): Observable<any> {
    return this.httpClient.get(`${baseUrl}${url}`);
  }

  public post(url: string, data: any): Observable<any> {
    return this.httpClient.post(`${baseUrl}${url}`, data);
  }

  public delete(url: string): Observable<any> {
    return this.httpClient.delete(`${baseUrl}${url}`);
  }

  public put(url: string, data: any): Observable<any> {
    return this.httpClient.put(`${baseUrl}${url}`, data);
  }
}
