export class CaregiverDTO {
    id: string;
    name: string;
    birthDate: Date;
    gender: string;
    address: string;
}