import { Injectable } from "@angular/core";

declare var SockJS;
declare var Stomp;

@Injectable()
export class MessageService {
    
    constructor() {
       // this.initializeWebSocketConnection();
    }

    public stompClient;
    public msg = [];

    showNotification(message) {
        console.log(message);
    }

    initializeWebSocketConnection(array: Array<any>) {
        const serverUrl = 'http://localhost:8080/notifications';
        const socket = new SockJS(serverUrl);
        this.stompClient = Stomp.over(socket);

        const that = this;
       
        this.stompClient.connect({}, function(frame) {
            that.stompClient.subscribe('/topic/notification', (message) => {
            if (message.body) {
                console.log("ATENTIE: ", JSON.parse(message.body));
                array.push(JSON.parse(message.body));
            }
            });
        });
    }

}