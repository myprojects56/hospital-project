import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { CaregiverDTO } from './caregiverDTO/caregiverDTO.model';
import { HttpService } from './http.service';
import { MedicationPlanDTO } from './medicationPlanDTO/medicationPlanDTO';
import { MedicationPlanDTOS } from './medicationPlanDTO/medicationPlanDTOS';
import { PatientDTO } from './patientDTO/patientDTO.model';

@Injectable()
export class PatientService {
  private httpService: HttpService;
  
  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public add(patientDTO: PatientDTO): Observable<string> {
    return this.httpService.post('/patient', patientDTO);
  }

  public getAll(): Observable<PatientDTO[]> {
      return this.httpService.get('/patient');
  }

  public update(patientDTO: PatientDTO): Observable<PatientDTO> {
    return this.httpService.post('/patient/update', patientDTO);
  }

  public delete(id: string): Observable<string> {
    return this.httpService.delete(`/patient/${id}`);
  }

  public getById(id: string): Observable<PatientDTO> {
    return this.httpService.get(`/patient/${id}`);
  }

  public getByCaregiverId(id: string): Observable<PatientDTO[]> {
    return this.httpService.get(`/patient/by-caregiver/${id}`);
  }

  public createMedicationPlan(id: string, medicationPlans: Array<MedicationPlanDTO>): Observable<string> {
    console.log(medicationPlans);
    return this.httpService.post(`/doctor/medication-plan/${id}`, medicationPlans);
  }
}