import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { HttpService } from '../http.service';
import { User } from './user.model';

@Injectable()
export class UserService {
    private httpService: HttpService;

    constructor(httpService: HttpService) {
        this.httpService = httpService;
    }

}