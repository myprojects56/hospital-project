export class NotificationDTO {
    patient_id: string;
    end: string;
    start: string;
    activity: string;
}