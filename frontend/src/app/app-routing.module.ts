import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaregiverComponent } from './main/caregiver/caregiver.component';
import { DoctorComponent } from './main/doctor/doctor.component';
import { LoginComponent } from './main/login/login.component';
import { MainComponent } from './main/main.component';
import { PatientComponent } from './main/patient/patient.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'doctor',
        component: DoctorComponent
      },
      {
        path: 'caregiver',
        component: CaregiverComponent
      },
      {
        path: 'patient',
        component: PatientComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
