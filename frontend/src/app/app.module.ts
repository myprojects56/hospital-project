import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpService } from './shared/http.service';
import { MainComponent } from './main/main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './main/login/login.service';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './main/login/login.component';
import { DoctorComponent } from './main/doctor/doctor.component';
import { CaregiverService } from './shared/caregiver.service';
import { MedicationService } from './shared/medication.service';
import { PatientService } from './shared/patient.service';
import { CaregiverComponent } from './main/caregiver/caregiver.component';
import { PatientComponent } from './main/patient/patient.component';
import { MessageService } from './shared/message.service';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    DoctorComponent,
    CaregiverComponent,
    PatientComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    HttpService,
    LoginService,
    CaregiverService,
    MedicationService,
    PatientService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
