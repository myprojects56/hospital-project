import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/shared/user/user.model';
import { LoginService } from './login.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
  })
  export class LoginComponent implements OnInit {

    public user: User = new User();
    public username: string;
    public password: string;

    private router: Router;
    private loginService: LoginService;

    ngOnInit(): void {
        console.log('pagina de login!!!!!');
    }

    constructor(loginService: LoginService, router: Router) {
      this.loginService = loginService;
      this.router = router;
    }

    public logIn(username, password) {

      this.user.username = username;
      this.user.password = password;
      console.log("user: ", this.user);

      this.loginService.performLogin(this.user).subscribe( (data) => {

        if (data.doctorDTO !== null) {
          localStorage.setItem('doctor', JSON.stringify(data.doctorDTO));
          this.router.navigateByUrl('/doctor');
        } else if (data.caregiverDTO !== null) {
          localStorage.setItem('caregiver', JSON.stringify(data.caregiverDTO));
          this.router.navigateByUrl('/caregiver');
        } else if (data.patientDTO !== null) {
          localStorage.setItem('patient', JSON.stringify(data.patientDTO));
          this.router.navigateByUrl('/patient');
        }
      });
      return false;
    }
      
  }