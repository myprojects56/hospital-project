import { Injectable } from "@angular/core";
import { HttpService } from 'src/app/shared/http.service';
import { User } from 'src/app/shared/user/user.model';

@Injectable()
export class LoginService {
  private httpService: HttpService;

  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public performLogin(user: User) {
      return this.httpService.post('/login', user);
  }
}