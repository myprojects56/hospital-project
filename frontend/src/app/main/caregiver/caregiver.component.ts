import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CaregiverService } from 'src/app/shared/caregiver.service';
import { CaregiverDTO } from 'src/app/shared/caregiverDTO/caregiverDTO.model';
import { MedicationService } from 'src/app/shared/medication.service';
import { PatientService } from 'src/app/shared/patient.service';
import { PatientDTO } from 'src/app/shared/patientDTO/patientDTO.model';

import { Client, Message } from '@stomp/stompjs';
import { MessageService } from 'src/app/shared/message.service';
import { NotificationDTO } from 'src/app/shared/notificationDTO/notificationDTO.model';

@Component({
    selector: 'caregiver',
    templateUrl: './caregiver.component.html',
    styleUrls: ['./caregiver.component.scss'],
  })
  export class CaregiverComponent implements OnInit {
    public caregiverDTO: CaregiverDTO;
    public patients: Array<PatientDTO> = new Array<PatientDTO>();
    public notifications: Array<NotificationDTO> = new Array<NotificationDTO>();

    private caregiverService: CaregiverService;
    private patientService: PatientService;
    private messageService: MessageService;

    ngOnInit(): void {
        this.caregiverDTO = JSON.parse(localStorage.getItem('caregiver'));

        this.patientService.getByCaregiverId(this.caregiverDTO.id).subscribe( (data) => {
            this.patients = data;
        })

    }

    constructor(caregiverService: CaregiverService, patientService: PatientService, messageService: MessageService) {
        this.caregiverService = caregiverService;
        this.patientService = patientService;
        this.messageService = messageService;

        this.messageService.initializeWebSocketConnection(this.notifications);
    }
        
      
}