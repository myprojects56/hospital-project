import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CaregiverService } from 'src/app/shared/caregiver.service';
import { CaregiverDTO } from 'src/app/shared/caregiverDTO/caregiverDTO.model';
import { DoctorDTO } from 'src/app/shared/doctorDTO/doctorDTO.model';
import { MedicationService } from 'src/app/shared/medication.service';
import { MedicationDTO } from 'src/app/shared/medicationDTO/medicationDTO.model';
import { MedicationPlanDTO } from 'src/app/shared/medicationPlanDTO/medicationPlanDTO';
import { MedicationPlanDTOS } from 'src/app/shared/medicationPlanDTO/medicationPlanDTOS';
import { PatientService } from 'src/app/shared/patient.service';
import { PatientDTO } from 'src/app/shared/patientDTO/patientDTO.model';

@Component({
    selector: 'doctor',
    templateUrl: './doctor.component.html',
    styleUrls: ['./doctor.component.scss'],
  })
  export class DoctorComponent implements OnInit {
    public doctorDTO: DoctorDTO;

    public newCaregiver: CaregiverDTO;
    public caregivers: Array<CaregiverDTO> = new Array<CaregiverDTO>();
    public updatedCaregiver: CaregiverDTO = new CaregiverDTO();

    public newMedication: MedicationDTO;
    public medications: Array<MedicationDTO> = new Array<MedicationDTO>();
    public updatedMedication: MedicationDTO = new MedicationDTO();

    public newPatient: PatientDTO;
    public patients: Array<PatientDTO> = new Array<PatientDTO>();
    public updatedPatient: PatientDTO = new PatientDTO();

    public newMedicationPlanMorning: MedicationPlanDTO;
    public newMedicationPlanNoon: MedicationPlanDTO;
    public newMedicationPlanEvening: MedicationPlanDTO;

    private router: Router;
    private caregiverService: CaregiverService;
    private medicationService: MedicationService;
    private patientService: PatientService;

    ngOnInit(): void {
        this.doctorDTO = JSON.parse(localStorage.getItem('doctor'));

        // INIT NEW
        this.newCaregiver = new CaregiverDTO();
        this.newMedication = new MedicationDTO();
        this.newPatient = new PatientDTO();
        this.newMedicationPlanMorning = new MedicationPlanDTO();
        this.newMedicationPlanNoon= new MedicationPlanDTO();
        this.newMedicationPlanEvening= new MedicationPlanDTO();

        this.updatedCaregiver.name = '';
        this.updatedCaregiver.birthDate = new Date();
        this.updatedCaregiver.address = '';
        this.updatedCaregiver.gender = "Male";

        this.updatedMedication.name = '';
        this.updatedMedication.sideEffects = '';
        this.updatedMedication.dose = 0;

        this.updatedPatient.name = '';
        this.updatedPatient.address = '';
        this.updatedPatient.medicalRecord = '';
        this.updatedPatient.gender = "Male";


        console.log('on init: ', this.updatedCaregiver);
    }

    constructor(caregiverService: CaregiverService, medicationService: MedicationService, patientService: PatientService, router: Router) {
      this.caregiverService = caregiverService;
      this.medicationService = medicationService;
      this.patientService = patientService;
      this.router = router;

      // GET ALL
      this.caregiverService.getAll().subscribe( (data) => {
          this.caregivers = data;
      });

      this.medicationService.getAll().subscribe( (data) => {
        this.medications = data;
      })

      this.patientService.getAll().subscribe( (data) => {
        this.patients = data;
      })
      
    }

    // ADD 
    addCaregiver(name, birthDate, gendre, address) {
      this.newCaregiver.name = name;
      this.newCaregiver.address = address;
      this.newCaregiver.birthDate = birthDate;
      this.newCaregiver.gender = gendre

      this.caregiverService.add(this.newCaregiver).subscribe( (data) => {
        this.caregiverService.getAll().subscribe( (data) => {
          this.caregivers = data;
        });
      })
    }

    addMedication(name, effects, dose) {
      this.newMedication.name = name;
      this.newMedication.dose = dose;
      this.newMedication.sideEffects = effects;

      this.medicationService.add(this.newMedication).subscribe( (data) => {
        this.medicationService.getAll().subscribe( (data) => {
          this.medications = data;
        })
      })

    }

    addPatient(name, birthDate, gendre, address, caregiverId, medicalRec) {
      this.newPatient.name = name;
      this.newPatient.address = address;
      this.newPatient.birthDate = birthDate;
      this.newPatient.gender = gendre;
      this.newPatient.medicalRecord = medicalRec;
      this.newPatient.doctor = this.doctorDTO.id;
      this.newPatient.caregiver = caregiverId;

      this.patientService.add(this.newPatient).subscribe( (data) => {
        this.patientService.getAll().subscribe( (data) => {
          this.patients = data;
        });
      })

    }

    // DELETE
    deleteCaregiver(id) {
      this.caregiverService.delete(id).subscribe( () => {
        this.caregiverService.getAll().subscribe( (data) => {
          this.caregivers = data;
        });
      })
    }

    deleteMedication(id) {
      this.medicationService.delete(id).subscribe( () => {
        this.medicationService.getAll().subscribe( (data) => {
          this.medications = data;
        });
      })
    }

    deletePatient(id) {
      this.patientService.delete(id).subscribe( () => {
        this.patientService.getAll().subscribe( (data) => {
          this.patients = data;
        });
      })
    }

    // UPDATE

    updateCaregiver(id, name, birthDate, gendre, address) {
      this.updatedCaregiver.name = name;
      this.updatedCaregiver.gender = gendre;
      this.updatedCaregiver.birthDate = birthDate;
      this.updatedCaregiver.address = address;

      this.caregiverService.update(this.updatedCaregiver).subscribe( (data1) => {
        this.caregiverService.getAll().subscribe( (data) => {
          this.caregivers = data;
        });
      })
    }

    updateMedication(id, name, effect, dose) {
      this.updatedMedication.name = name;
      this.updatedMedication.sideEffects = effect;
      this.updatedMedication.dose = dose;

      console.log("asta salvez: ", this.updatedMedication)

      this.medicationService.update(this.updatedMedication).subscribe( (data1) => {
        this.medicationService.getAll().subscribe( (data) => {
          this.medications = data;
        });
      })
    }

    updatePatient(id, name, birthDate, gendre, address, caregiver, record) {
      this.updatedPatient.name = name;
      this.updatedPatient.gender = gendre;
      this.updatedPatient.birthDate = birthDate;
      this.updatedPatient.address = address;
      this.updatedPatient.caregiver = caregiver;
      this.updatedPatient.medicalRecord = record;
      this.updatedPatient.doctor = this.doctorDTO.id

      console.log(this.updatedPatient);

      this.patientService.update(this.updatedPatient).subscribe( (data1) => {
        this.patientService.getAll().subscribe( (data) => {
          this.patients = data;
        });
      })
    }


    //CREATE MEDICATION PLAN
    createMedication(patientId, medicationId, morning, noon, evening, startDate, endDate) {

      var medicationPlans: Array<MedicationPlanDTO> = new Array<MedicationPlanDTO>();
      var medicationPlan = new MedicationPlanDTOS();

      if (morning.checked) {
        this.newMedicationPlanMorning.doctor = this.doctorDTO.id;
        this.newMedicationPlanMorning.patient = patientId;
        this.newMedicationPlanMorning.medication = medicationId;
        this.newMedicationPlanMorning.intervalStart = startDate;
        this.newMedicationPlanMorning.intervalEnd = endDate;
        this.newMedicationPlanMorning.partOfDay = "morning";
        this.newMedicationPlanMorning.period = "0";
        medicationPlans.push(this.newMedicationPlanMorning);
      }

      if (noon.checked) {
        this.newMedicationPlanNoon.doctor = this.doctorDTO.id;
        this.newMedicationPlanNoon.patient = patientId;
        this.newMedicationPlanNoon.medication = medicationId;
        this.newMedicationPlanNoon.intervalStart = startDate;
        this.newMedicationPlanNoon.intervalEnd = endDate;
        this.newMedicationPlanNoon.partOfDay = "noon";
        this.newMedicationPlanNoon.period = "0";
        medicationPlans.push(this.newMedicationPlanNoon);
      }

      if (evening.checked) {
        this.newMedicationPlanEvening.doctor = this.doctorDTO.id;
        this.newMedicationPlanEvening.patient = patientId;
        this.newMedicationPlanEvening.medication = medicationId;
        this.newMedicationPlanEvening.intervalStart = startDate;
        this.newMedicationPlanEvening.intervalEnd = endDate;
        this.newMedicationPlanEvening.partOfDay = "evening";
        this.newMedicationPlanEvening.period = "0";
        medicationPlans.push(this.newMedicationPlanEvening);
      }

      medicationPlan.medicationPlanDTOS = medicationPlans;

      this.patientService.createMedicationPlan(patientId, medicationPlans).subscribe( (data) => {
        console.log(data);
      });
      
    }


    // ON CHANGE
    onChangeCaregiver(id) {
      this.caregiverService.getById(id).subscribe( (data) => {
        this.updatedCaregiver = data;
      })
    }

    onChangeMedication(id) {
      this.medicationService.getById(id).subscribe( (data) => {
        this.updatedMedication = data;
      })
    }

    onChangePatient(id) {
      this.patientService.getById(id).subscribe( (data) => {
        this.updatedPatient = data;
      })
    }
      
  }