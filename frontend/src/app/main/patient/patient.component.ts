import { Component, OnInit } from '@angular/core';
import { PatientDTO } from 'src/app/shared/patientDTO/patientDTO.model';

@Component({
    selector: 'patient',
    templateUrl: './patient.component.html',
    styleUrls: ['./patient.component.scss'],
  })
  export class PatientComponent implements OnInit {
    public patientDTO: PatientDTO;

    ngOnInit(): void {
        this.patientDTO = JSON.parse(localStorage.getItem('patient'));
    }
      
  }