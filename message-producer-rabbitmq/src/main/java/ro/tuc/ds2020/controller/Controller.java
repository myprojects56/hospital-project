package ro.tuc.ds2020.controller;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;
import ro.tuc.ds2020.DTO.ActivityDTO;
import ro.tuc.ds2020.producer.Producer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

@RestController
public class Controller {

    @Autowired
    private Producer producer;

    @Scheduled(fixedDelay = 1000)
    public void readFile() throws FileNotFoundException, InterruptedException {

        File file = new File("activity.txt");
        Scanner readFile = new Scanner(file);

        while(readFile.hasNextLine()) {
            String data = readFile.nextLine();

            String[] dataFields = data.split("\t+");
            ActivityDTO activityDTO = new ActivityDTO();

            activityDTO.setPatient_id("7a73aeb9-17d8-47bf-9a65-aadb41b37f69");
            activityDTO.setStart(dataFields[0]);
            activityDTO.setEnd(dataFields[1]);
            activityDTO.setActivity(dataFields[2]);

            JSONObject jsonData = new JSONObject();
            jsonData.put("patient_id", activityDTO.getPatient_id());
            jsonData.put("start", activityDTO.getStart());
            jsonData.put("end", activityDTO.getEnd());
            jsonData.put("activity", activityDTO.getActivity());

            producer.produceMsg(jsonData.toString());

            Thread.sleep(3000);
        }

    }

}
