package ro.tuc.ds2020.DTO;

public class ActivityDTO {
    private String patient_id;
    private String activity;
    private String start;
    private String end;

    public ActivityDTO() {}
    public ActivityDTO(String id, String start, String end, String activity) {
        this.patient_id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
