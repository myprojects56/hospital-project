package ro.tuc.ds2020.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivityDTO;

@Component
public class MessageService {

    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public MessageService(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public void sendNotifcation(ActivityDTO activityDTO) {
        simpMessagingTemplate.convertAndSend("/topic/notification", activityDTO);
    }

}
