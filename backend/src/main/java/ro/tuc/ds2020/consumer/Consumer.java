package ro.tuc.ds2020.consumer;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.message.ObjectMessage;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivityDTO;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class Consumer implements MessageListener {

    @Autowired
    private MessageService messageService;

    @Override
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        System.out.println("Received Message: " + message);
        ObjectMapper mapper = new ObjectMapper();

        try {
            ActivityDTO activityDTO = mapper.readValue(message.getBody(), ActivityDTO.class);
            System.out.println(activityDTO.toString());

            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startTime = LocalDateTime.parse(activityDTO.getStart(), dateFormat);
            LocalDateTime endTime = LocalDateTime.parse(activityDTO.getEnd(), dateFormat);

            if ( activityDTO.getActivity().equals("Leaving") && Duration.between(startTime, endTime).toHours() > 5) {
                System.out.println("ATENTIE!!! : " + activityDTO.toString());
                messageService.sendNotifcation(activityDTO);
            }
            else if ( activityDTO.getActivity().equals("Sleeping") && Duration.between(startTime, endTime).toHours() > 7) {
                System.out.println("ATENTIE!!! : " + activityDTO.toString());
                messageService.sendNotifcation(activityDTO);
            }
            else if ( (activityDTO.getActivity().equals("Showering") || activityDTO.getActivity().equals("Toileting")) && Duration.between(startTime, endTime).toMinutes() > 30) {
                System.out.println("ATENTIE!!! : " + activityDTO.toString());
                messageService.sendNotifcation(activityDTO);
            }


        } catch (Exception e) {
            System.out.println("No se puede!");
        }

    }
}
