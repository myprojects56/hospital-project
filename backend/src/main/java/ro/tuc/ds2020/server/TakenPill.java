package ro.tuc.ds2020.server;

import java.io.Serializable;

public class TakenPill implements Serializable {
    private Boolean isOnTime;

    public TakenPill() {}
    public TakenPill(Boolean isOnTime) {
        this.isOnTime = isOnTime;
    }

    public Boolean getOnTime() {
        return isOnTime;
    }

    public void setOnTime(Boolean onTime) {
        isOnTime = onTime;
    }
}
