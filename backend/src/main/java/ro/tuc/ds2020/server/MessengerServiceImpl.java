package ro.tuc.ds2020.server;

import com.google.gson.Gson;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AutoJsonRpcServiceImpl
public class MessengerServiceImpl implements MessengerService {


    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;

    @Autowired
    public MessengerServiceImpl(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
    }

    @Override
    public String sendMessage(String clientMessage) {
        System.out.println("========== Server Side ==========");
        System.out.println("Incoming Message: " + clientMessage);
        return "Message: " + clientMessage + " ::Response time -> " + new Date();
    }

    @Override
    public String sendPatientId(String patientId) throws RemoteException {
        System.out.println("========== Server Side ==========");
        System.out.println("Incoming Message: " + patientId);

        Optional<List<MedicationPlan>> medicationPlanOptional = medicationPlanRepository.findByPatientId(patientId);
        if(!medicationPlanOptional.isPresent()) {
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + patientId);
        }

        List<MedicationPlan> medicationPlans = medicationPlanOptional.get();

        List<ResponseMedication> response = new ArrayList<>();

        for(MedicationPlan medicationPlan: medicationPlans) {
            ResponseMedication medication = new ResponseMedication();
            medication.setId(medicationPlan.getId());
            medication.setName(medicationPlan.getPeriod());
            medication.setIntervalStart(medicationPlan.getIntervalStart());
            medication.setIntervalEnd(medicationPlan.getIntervalEnd());


            if (medicationPlan.getPartOfDay().equals("morning")) {
                medication.setHour("11:15");
            } else if (medicationPlan.getPartOfDay().equals("noon")) {
                medication.setHour("16:00");
            } else if (medicationPlan.getPartOfDay().equals("evening")) {
                medication.setHour("19:22");
            }

            response.add(medication);
        }

        Gson res = new Gson();
        String object = res.toJson(response);
        System.out.println(object);

        return object;
    }

    @Override
    public String pillTaken(TakenPill takenPill) throws RemoteException {
        if(takenPill.getOnTime()) {
            return "Pill taken on time!";
        }
        return "Attention! ";
    }

}
