package ro.tuc.ds2020.server;

import java.io.Serializable;
import java.util.Date;

public class ResponseMedication implements Serializable {

    private String id;
    private String name;
    private Date intervalStart;
    private Date intervalEnd;
    private String hour;

    public ResponseMedication() {}

    public ResponseMedication(String id, String name, Date start, Date end, String hour) {
        this.id = id;
        this.name = name;
        this.intervalStart = start;
        this.intervalEnd = end;
        this.hour = hour;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getIntervalStart() {
        return intervalStart;
    }

    public void setIntervalStart(Date intervalStart) {
        this.intervalStart = intervalStart;
    }

    public Date getIntervalEnd() {
        return intervalEnd;
    }

    public void setIntervalEnd(Date intervalEnd) {
        this.intervalEnd = intervalEnd;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        return "ResponseMedication{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", intervalStart=" + intervalStart +
                ", intervalEnd=" + intervalEnd +
                ", hour='" + hour + '\'' +
                '}';
    }
}
