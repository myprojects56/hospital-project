package ro.tuc.ds2020.server;

import java.io.Serializable;

public class Ana implements Serializable {
    private String name;
    private String age;

    public Ana() {}

    public Ana( String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Ana{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
