package ro.tuc.ds2020.server;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

@JsonRpcService("/hello")
public interface MessengerService extends Remote {
    String sendMessage(@JsonRpcParam(value="clientMessage") String clientMessage) throws RemoteException;

    String sendPatientId(@JsonRpcParam(value="patientId") String patientId) throws RemoteException;

    String pillTaken(@JsonRpcParam(value="takenPill")TakenPill takenPill) throws RemoteException;
}


