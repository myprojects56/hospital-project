package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="medicationplanchart")
public class MedicationPlanChart implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "medicationPlanId")
    private MedicationPlan medicationPlan;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "medicationId")
    private Medication medication;

    public MedicationPlanChart () {}

    public MedicationPlanChart (MedicationPlan medicationPlan, Medication medication) {
        this.medication = medication;
        this.medicationPlan = medicationPlan;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanChart that = (MedicationPlanChart) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(medicationPlan, that.medicationPlan) &&
                Objects.equals(medication, that.medication);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medicationPlan, medication);
    }
}
