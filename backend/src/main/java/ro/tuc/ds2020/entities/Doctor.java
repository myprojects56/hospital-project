package ro.tuc.ds2020.entities;

import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="doctor")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

//    @OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY)
//    private List<Patient> patients = new ArrayList<Patient>();
//
//    @OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY)
//    private List<MedicationPlan> medicationPlans = new ArrayList<MedicationPlan>();

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private User user;

    public Doctor() {}

    public Doctor(String name) {
        this.name = name;
        this.id = UUID.randomUUID().toString();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<Patient> getPatients() {
//        return patients;
//    }
//
//    public void setPatients(List<Patient> patients) {
//        this.patients = patients;
//    }
//
//    public List<MedicationPlan> getMedicationPlans() {
//        return medicationPlans;
//    }
//
//    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
//        this.medicationPlans = medicationPlans;
//    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
