package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="medication")
public class Medication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sideEffects", nullable = false)
    private String sideEffects;

    @Column(name = "dose", nullable = false)
    private int dose;

    @OneToMany(mappedBy = "medication", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<MedicationPlanChart> medicationPlanCharts = new ArrayList<MedicationPlanChart>();

    @OneToMany(mappedBy = "medication", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private List<MedicationPlan> medicationPlans = new ArrayList<MedicationPlan>();


    public Medication() {
    }

    public Medication(String name, String sideEffects, int dose) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dose = dose;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sideEffects='" + sideEffects + '\'' +
                '}';
    }
}
