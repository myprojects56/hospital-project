package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlanChart;

import java.util.List;
import java.util.UUID;

public interface MedicationPlanChartRepository extends JpaRepository<MedicationPlanChart, UUID> {
    MedicationPlanChart findByMedicationId(String id);
    List<MedicationPlanChart> findByMedicationPlanId(String id);
    void deleteById(String id);
}
