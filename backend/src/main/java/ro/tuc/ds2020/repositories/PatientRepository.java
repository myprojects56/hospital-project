package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PatientRepository extends JpaRepository<Patient, String> {
    Patient findByName(String name);
    Optional<Patient> findById(String id);
    void deleteById(String id);
    Optional<Patient> findByUserId(String id);
    Optional<List<Patient>> findByCaregiverId(String id);
}
