package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Doctor;

import java.util.Optional;
import java.util.UUID;

public interface DoctorRepository extends JpaRepository<Doctor, String> {
    Doctor findByName(String name);
    Optional<Doctor> findById(String id);
    Optional<Doctor> findByUserId(String id);
}
