package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.services.UserService;


@RestController
@CrossOrigin
public class IndexController {

    private final UserService userService;

    private final DoctorService doctorService;
    private final CaregiverService caregiverService;
    private final PatientService patientService;

    @Autowired
    public IndexController(UserService userService, DoctorService doctorService, PatientService patientService, CaregiverService caregiverService) {
        this.userService = userService;
        this.caregiverService = caregiverService;
        this.patientService = patientService;
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/")
    public ResponseEntity<String> getStatus() {
        return new ResponseEntity<>("City APP Service is running...", HttpStatus.OK);
    }

    @PostMapping(value = "/create-doctor-account")
    public ResponseEntity<String> createDoctorAccount(@RequestBody RegisterDTO registerDTO) {
        User user1 = new User(registerDTO.getUsername(), registerDTO.getPassword());
        Doctor doctor1 = new Doctor(registerDTO.getName());
        userService.insert(user1);
        doctor1.setUser(user1);
        user1.setDoctor(doctor1);

        doctorService.insert(doctor1);
        return new ResponseEntity<>("Account created!", HttpStatus.OK);
    }

    @PostMapping(value = "/create-caregiver-account")
    public ResponseEntity<String> createCaregiverAccount(@RequestBody RegisterDTO registerDTO) {
        Caregiver caregiver = new Caregiver(registerDTO.getName(), registerDTO.getBirthDate(), registerDTO.getGender(), registerDTO.getAddress());
        User user1 = new User(registerDTO.getUsername(), registerDTO.getPassword());
        caregiver.setUser(user1);
        user1.setCaregiver(caregiver);
        userService.insert(user1);
        caregiverService.insert(caregiver);
        return new ResponseEntity<>("Account created!", HttpStatus.OK);
    }

    @PostMapping(value = "/create-patient-account")
    public ResponseEntity<String> createPatientAccount(@RequestBody RegisterDTO registerDTO) {
        Patient patient = new Patient(registerDTO.getName(), registerDTO.getBirthDate(), registerDTO.getGender(), registerDTO.getAddress(), registerDTO.getMedicalRecord());
        User user1 = new User(registerDTO.getUsername(), registerDTO.getPassword());

        DoctorDTO d = doctorService.findDoctorById("133e78d1-f924-4e6c-b665-88af9475f822");
        CaregiverDTO c = caregiverService.findCaregiverById("0b41d156-c1aa-46df-985d-6ee5d8b288f0");

        patient.setUser(user1);
        user1.setPatient(patient);
        userService.insert(user1);
        patientService.insert(patient, d, c);
        return new ResponseEntity<>("Account created!", HttpStatus.OK);
    }

    @PostMapping(value = "/login")
    public ResponseEntity loginUser(@RequestBody User user) {
        System.out.println("aiciiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        User registeredUser = userService.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        ResponseDTO responseDTO;

        try {
            DoctorDTO doctorDTO = doctorService.findDoctorByUserId(registeredUser.getId());
            responseDTO = new ResponseDTO("Hello Doctor!");
            responseDTO.setDoctorDTO(doctorDTO);

        } catch (Exception e) {

            try {
                CaregiverDTO caregiverDTO = caregiverService.findCaregiverByUserId(registeredUser.getId());
                responseDTO = new ResponseDTO("Hello Caregiver!");
                responseDTO.setCaregiverDTO(caregiverDTO);
            } catch (Exception e1) {

                try {
                    PatientDTO patientDTO = patientService.findPatientByUserId(registeredUser.getId());
                    responseDTO = new ResponseDTO("Hello Patient!");
                    responseDTO.setPatientDTO(patientDTO);
                } catch (Exception e2) {
                    responseDTO = new ResponseDTO("Invalid Credentials!!!");
                }
            }
        }

        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }
}
