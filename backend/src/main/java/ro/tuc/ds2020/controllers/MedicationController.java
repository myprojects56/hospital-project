package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.MedicationService;
import ro.tuc.ds2020.services.PatientService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;
    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationController(MedicationService medicationService, PatientService patientService, MedicationPlanService medicationPlanService) {
        this.medicationService = medicationService;
        this.patientService = patientService;
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> medicationDTOS = medicationService.findMedications();
        return new ResponseEntity<>(medicationDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedicationById(@PathVariable("id") String medicationtId) {
        MedicationDTO medicationDTO = medicationService.findMedicationById(medicationtId);
        return new ResponseEntity<>(medicationDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<MedicationDTO> insertMedication(@RequestBody MedicationDTO medicationDTO) {
        String medicationId = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<MedicationDTO> updateMedication(@RequestBody MedicationDTO medicationDTO) {
        medicationService.update(medicationDTO);
        return new ResponseEntity<>(medicationDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> deleteMedication(@PathVariable("id") String medicationId) {
        MedicationDTO medication = new MedicationDTO();
        medicationService.delete(medicationId);
        return  new ResponseEntity<>(medication, HttpStatus.OK);
    }

    @GetMapping(value = "/user-medication-plan/{id}")
    public ResponseEntity<List<MedicationPlanDTO>> getPatientMedicationPlan(@PathVariable("id") String patientId) {
        Patient patient = PatientBuilder.toEntity(patientService.findPatientById(patientId));
        patient.setId(patientId);

        List<MedicationPlan> medicationPlans =  medicationPlanService.findMedicationPlanByUserId(patientId);
        List<MedicationPlanDTO> medicationPlanDTOS = new ArrayList<MedicationPlanDTO>();
        for(MedicationPlan medicationPlan : medicationPlans) {
            medicationPlanDTOS.add(MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan));
        }
        return  new ResponseEntity<>(medicationPlanDTOS, HttpStatus.OK);
    }
}












