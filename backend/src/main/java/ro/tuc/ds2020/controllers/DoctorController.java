package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.services.*;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {
    private final DoctorService doctorService;
    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;
    private final MedicationService medicationService;
    private final MedicationPlanChartService medicationPlanChartService;

    @Autowired
    public DoctorController(DoctorService doctorService, PatientService patientService, MedicationPlanService medicationPlanService, MedicationService medicationService, MedicationPlanChartService medicationPlanChartService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.medicationPlanService = medicationPlanService;
        this.medicationService = medicationService;
        this.medicationPlanChartService = medicationPlanChartService;
    }

    @GetMapping()
    public ResponseEntity<List<DoctorDTO>> getDoctors() {
        List<DoctorDTO> doctorDTOS = doctorService.findDoctors();
        return new ResponseEntity<>(doctorDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DoctorDTO> getDoctorById(@PathVariable("id") String doctorId) {
        System.out.println("id: "+ doctorId);
        DoctorDTO doctorDTO = doctorService.findDoctorById(doctorId);
        return new ResponseEntity<>(doctorDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<String> insertDoctor(@RequestBody DoctorDTO doctorDTO) {
        //String doctorId = doctorService.insert(doctorDTO);
        String doctorId = doctorService.insert(DoctorBuilder.toEntity(doctorDTO));

        return new ResponseEntity<>(doctorId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteDoctor(@PathVariable("id") UUID doctorId) {
        doctorService.delete(doctorId.toString());
        return new ResponseEntity<>(doctorId.toString(), HttpStatus.OK);
    }

    @PostMapping(value = "/medication-plan/{id}")
    public ResponseEntity<ResponseDTO> createMedicationPlan(@PathVariable("id") String patientId, @RequestBody List<MedicationPlanDTO> medicationPlanDTOS ){

        Patient patient = PatientBuilder.toEntity(patientService.findPatientById(patientId));
        patient.setId(patientId);
        Doctor doctor = DoctorBuilder.toEntity(doctorService.findDoctorById("133e78d1-f924-4e6c-b665-88af9475f822"));
        doctor.setId("133e78d1-f924-4e6c-b665-88af9475f822");
        System.out.println(doctor.toString());

        for ( MedicationPlanDTO medicationPlanDTO: medicationPlanDTOS) {
            Medication medication = MedicationBuilder.toEntity(medicationService.findMedicationById(medicationPlanDTO.getMedication()));
            medication.setId(medicationPlanDTO.getMedication());
            MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO, patient, doctor, medication);
            medicationPlan.setPeriod(medication.getName());

            medicationPlanService.insert(medicationPlan);
        }

        return new ResponseEntity<>(new ResponseDTO("Success"), HttpStatus.OK);
    }


}
