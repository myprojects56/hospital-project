package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.services.CaregiverService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {
    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> caregiverDTOS = caregiverService.findCaregivers();

        return new ResponseEntity<>(caregiverDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiverById(@PathVariable("id") String caregiverId) {
        CaregiverDTO caregiverDTO = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<CaregiverDTO> insertCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        caregiverService.insert(CaregiverBuilder.toEntity(caregiverDTO));
        System.out.println(CaregiverBuilder.toEntity(caregiverDTO));
        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<CaregiverDTO> updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        caregiverService.update(caregiverDTO);
        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> deleteCaregiver(@PathVariable("id") String caregiverId) {
        CaregiverDTO caregiverDTO = new CaregiverDTO();
        caregiverService.delete(caregiverId);
        return  new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }


}















