package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final DoctorService doctorService;
    private final CaregiverService caregiverService;

    @Autowired
    public PatientController(PatientService patientService, DoctorService doctorService, CaregiverService caregiverService) {
        this.patientService = patientService;
        this.doctorService = doctorService;
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO >> getPatients() {
        List<PatientDTO> patientDTOS = patientService.findPatients();
        return new ResponseEntity<>(patientDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatientById(@PathVariable("id") String patientId) {
        PatientDTO patientDTO = patientService.findPatientById(patientId);
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<PatientDTO> insertPatient(@RequestBody PatientDTO patientDTO) {
        System.out.println("##################################");

        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient.setId(UUID.randomUUID().toString());
        patientService.insert(patient, doctorService.findDoctorById(patientDTO.getDoctor()), caregiverService.findCaregiverById(patientDTO.getCaregiver()));
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<PatientDTO> updatePatient(@RequestBody PatientDTO patientDTO) {
        patientService.update(patientDTO, doctorService.findDoctorById(patientDTO.getDoctor()), caregiverService.findCaregiverById(patientDTO.getCaregiver()));
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> deletePatient(@PathVariable("id") String patientId) {
        patientService.delete(patientId);
        PatientDTO patientDTO = new PatientDTO();
        return  new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/by-caregiver/{id}")
    public ResponseEntity<List<PatientDTO>> getPatientsByCaregiver(@PathVariable("id") String caregiverId) {
        return  new ResponseEntity<>(patientService.findPatientByCaregiverId(caregiverId), HttpStatus.OK);
    }

}










