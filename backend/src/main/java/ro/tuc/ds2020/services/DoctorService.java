package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.DoctorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public List<DoctorDTO> findDoctors() {
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorBuilder::toDoctorDTO)
                .collect(Collectors.toList());
    }

    public DoctorDTO findDoctorById(String id) {
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        if(!doctorOptional.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }
        return  DoctorBuilder.toDoctorDTO(doctorOptional.get());
    }

//    public String insert(DoctorDTO doctorDTO) {
//        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
//        doctor = doctorRepository.save(doctor);
//        LOGGER.debug("Person with id {} was inserted in db", doctor.getId());
//        return doctor.getId();
//    }

    public String insert(Doctor doctor) {
        doctor = doctorRepository.save(doctor);
        LOGGER.debug("Person with id {} was inserted in db", doctor.getId());
        return doctor.getId();
    }

    public void delete(String id) {
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        if(!doctorOptional.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db -> cannot delete", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }
        doctorRepository.delete(doctorOptional.get());
        LOGGER.debug("Person with id {} was deleted from db", id);
    }

    public DoctorDTO findDoctorByUserId(String id) {
        Optional<Doctor> doctorOptional = doctorRepository.findByUserId(id);
        if(!doctorOptional.isPresent()) {
            LOGGER.error("Doctor with user_id {} was not found in db", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }
        return  DoctorBuilder.toDoctorDTO(doctorOptional.get());
    }
}
