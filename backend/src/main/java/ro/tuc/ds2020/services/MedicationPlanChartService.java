package ro.tuc.ds2020.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.MedicationPlanChart;
import ro.tuc.ds2020.repositories.MedicationPlanChartRepository;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;

import java.util.List;

@Service
public class MedicationPlanChartService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanChartService.class);
    private final MedicationPlanChartRepository medicationPlanChartRepository;

    @Autowired
    public MedicationPlanChartService(MedicationPlanChartRepository medicationPlanChartRepository) {
        this.medicationPlanChartRepository = medicationPlanChartRepository;
    }

    public List<MedicationPlanChart> findMedicationPlanChartByMedicationPlanId(String id) {
        return medicationPlanChartRepository.findByMedicationPlanId(id);
    }

    public void insert(MedicationPlanChart medicationPlanChart) {
        medicationPlanChartRepository.save(medicationPlanChart);
    }
}
