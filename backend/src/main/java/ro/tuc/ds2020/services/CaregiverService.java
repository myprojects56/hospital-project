package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(String id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(caregiverOptional.get());
    }

//    public String insert(CaregiverDTO caregiverDTO) {
//        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
//        caregiver = caregiverRepository.save(caregiver);
//        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
//        return caregiver.getId();
//    }

    public String insert(Caregiver caregiver) {
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public CaregiverDTO update(CaregiverDTO updatedCaregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(updatedCaregiverDTO);
        caregiver.setId(updatedCaregiverDTO.getId());
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was updated", caregiver.getId());
        return CaregiverBuilder.toCaregiverDTO(caregiver);
    }

    public void delete(String id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db -> cannot delete", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        caregiverRepository.delete(caregiverOptional.get());
        LOGGER.debug("Person with id {} was deleted from db", id);
    }

    public CaregiverDTO findCaregiverByUserId(String id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findByUserId(id);
        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with user_id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(caregiverOptional.get());
    }
}
