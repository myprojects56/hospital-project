package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientDTO> findPatients() {
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(String id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(patientOptional.get());
    }

//    public String insert(PatientDTO patientDTO) {
//        Patient patient = PatientBuilder.toEntity(patientDTO);
//        patient = patientRepository.save(patient);
//        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
//        return patient.getId();
//    }

    public String insert(Patient patient, DoctorDTO doctorDTO, CaregiverDTO caregiverDTO) {
//        System.out.println("-----------caregiver"+ caregiver);
        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        doctor.setId(doctorDTO.getId());
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver.setId(caregiverDTO.getId());

        patient.setDoctor(doctor);
        patient.setCaregiver(caregiver);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public PatientDTO update(PatientDTO patientDTO, DoctorDTO doctorDTO, CaregiverDTO caregiverDTO) {
        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        doctor.setId(doctorDTO.getId());
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver.setId(caregiverDTO.getId());

        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient.setId(patientDTO.getId());
        patient.setDoctor(doctor);
        patient.setCaregiver(caregiver);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was updated", patient.getId());
        return PatientBuilder.toPatientDTO(patient);
    }

    public void delete(String id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db -> cannot delete", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        patientRepository.deleteById(id);
        LOGGER.debug("Patient with id {} was deleted from db", id);
    }

    public PatientDTO findPatientByUserId(String id) {
        Optional<Patient> patientOptional = patientRepository.findByUserId(id);
        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with user_id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(patientOptional.get());
    }

    public List<PatientDTO> findPatientByCaregiverId(String id) {
        Optional<List<Patient>> patientOptional = patientRepository.findByCaregiverId(id);
        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with user_id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        List<PatientDTO> patientDTOS = new ArrayList<>();
        for(Patient patient: patientOptional.get()) {
            patientDTOS.add(PatientBuilder.toPatientDTO(patient));
        }
        return patientDTOS;
    }
}
