package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getIntervalStart(), medicationPlan.getIntervalEnd(), medicationPlan.getPartOfDay(), medicationPlan.getPeriod(), medicationPlan.getDoctor().getId(), medicationPlan.getPatient().getId(), medicationPlan.getMedication().getId());
    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO, Patient patient, Doctor doctor, Medication medication) {
        return new MedicationPlan(medicationPlanDTO.getIntervalStart(),
                                    medicationPlanDTO.getIntervalEnd(),
                                    medicationPlanDTO.getPartOfDay(),
                                    medicationPlanDTO.getPeriod(),
                                    doctor,
                                    patient,
                                    medication);
    }
}
