package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanDTOS {
    private List<MedicationPlanDTO> medicationPlanDTOS = new ArrayList<MedicationPlanDTO>();

    public MedicationPlanDTOS() {}

    public List<MedicationPlanDTO> getMedicationPlanDTOS() {
        return medicationPlanDTOS;
    }

    public void setMedicationPlanDTOS(List<MedicationPlanDTO> medicationPlanDTOS) {
        this.medicationPlanDTOS = medicationPlanDTOS;
    }
}
