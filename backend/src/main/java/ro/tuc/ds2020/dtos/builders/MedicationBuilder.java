package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Person;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId(), medication.getName(),medication.getSideEffects(), medication.getDose());
    }

    public static Medication toEntity(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDose());
    }
}
