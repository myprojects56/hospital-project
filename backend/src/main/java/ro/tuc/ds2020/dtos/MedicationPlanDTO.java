package ro.tuc.ds2020.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Patient;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> {

    private String id;
    private Date intervalStart;
    private Date intervalEnd;
    private String partOfDay;
    private String period;
    private String doctor;
    private String patient;
    private String medication;

    private List<MedicationDTO> medicationList;

    public MedicationPlanDTO() {}

    public MedicationPlanDTO(String id, Date intervalStart, Date intervalEnd, String partOfDay, String period, String doctor, String patient, String medication) {
        this.doctor = doctor;
        this.id = id;
        this.intervalStart = intervalStart;
        this.intervalEnd = intervalEnd;
        this.partOfDay = partOfDay;
        this.period = period;
        this.patient = patient;
        this.medication = medication;
    }

    public List<MedicationDTO> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<MedicationDTO> medicationList) {
        this.medicationList = medicationList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getIntervalStart() {
        return intervalStart;
    }

    public void setIntervalStart(Date intervalStart) {
        this.intervalStart = intervalStart;
    }

    public Date getIntervalEnd() {
        return intervalEnd;
    }

    public void setIntervalEnd(Date intervalEnd) {
        this.intervalEnd = intervalEnd;
    }

    public String getPartOfDay() {
        return partOfDay;
    }

    public void setPartOfDay(String partOfDay) {
        this.partOfDay = partOfDay;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationPlanDTO that = (MedicationPlanDTO) o;
        return Objects.equals(id, that.id) &
                Objects.equals(period, that.period) &&
                Objects.equals(doctor, that.doctor) &&
                Objects.equals(patient, that.patient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, period, doctor, patient);
    }
}
