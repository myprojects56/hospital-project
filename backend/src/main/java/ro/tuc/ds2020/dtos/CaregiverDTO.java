package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> {
    private String id;
    private String name;
    private Date birthDate;
    private String gender;
    private String address;

    public CaregiverDTO() {}

    public CaregiverDTO (String id, String name, Date birthDate, String gender, String address) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return birthDate.compareTo(caregiverDTO.birthDate) == 0 &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(address, caregiverDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address);
    }
}
