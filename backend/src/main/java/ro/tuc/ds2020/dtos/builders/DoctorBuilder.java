package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Person;

public class DoctorBuilder {

    private DoctorBuilder() {}

    public static DoctorDTO toDoctorDTO(Doctor doctor) {
        return new DoctorDTO(doctor.getId(), doctor.getName());
    }

    public static Doctor toEntity(DoctorDTO doctorDTO) {
        return new Doctor(doctorDTO.getName());
    }
}
