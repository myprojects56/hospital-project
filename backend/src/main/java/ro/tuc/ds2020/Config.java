package ro.tuc.ds2020;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImplExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.server.MessengerService;
import ro.tuc.ds2020.server.MessengerServiceImpl;

@Configuration
public class Config {

    @Autowired
    MedicationPlanRepository medicationPlanRepository;

    @Autowired
    MedicationRepository medicationRepository;

//    @Bean
//    RemoteExporter registryRMIExporter() {
//        RmiServiceExporter exporter = new RmiServiceExporter();
//        exporter.setServiceName("helloworldrmi");
//        exporter.setServiceInterface(MessengerService.class);
//        exporter.setService(new MessengerServiceImpl(medicationPlanRepository, medicationRepository));
//
//        return exporter;
//    }

    @Bean
    public static AutoJsonRpcServiceImplExporter autoJsonRpcServiceImplExporter() {
        AutoJsonRpcServiceImplExporter exporter = new AutoJsonRpcServiceImplExporter();
        return exporter;
    }
}
